package TelephoneOffice.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService service;
    /*private final CustomerRepository repository;

    public CustomerController(CustomerRepository repository) {
        this.repository = repository;
    }*/

    @GetMapping("/customers")
    public ResponseEntity<List<CustomerDTO>> findAll() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findAll());
    }
   /* public List<Customer> all() {
        return repository.findAll();
    }*/

    @GetMapping("/customers/{id}")
    public ResponseEntity<CustomerDTO> findById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.FOUND)
                .body(service.findById(id));
    }
    /*Customer one(@PathVariable Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new CustomerNotFoundException(id));
    }*/

    @PostMapping("/customers/save")
    public ResponseEntity<CustomerDTO> newCustomer(@RequestBody CustomerDTO customerDTO) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(service.save(customerDTO));
    }
    /*public Customer newCustomer(@RequestBody Customer newCustomer) {
        return repository.save(newCustomer);
    }*/

    @PutMapping("/customers/update/{id}")
    public ResponseEntity<CustomerDTO> replaceCustomer(@RequestBody CustomerDTO customerDTO, @PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.update(customerDTO, id));
    }
    /*Customer replaceCustomer(@RequestBody Customer newCustomer, @PathVariable Long id) {

        return repository.findById(id)
                .map(e -> {
                    e.setName(newCustomer.getName());
                    return repository.save(e);
                })
                .orElseGet(() -> {
                    newCustomer.setId(id);
                    return repository.save(newCustomer);
                });
    }*/

    @DeleteMapping("/customers/delete/{id}")
    public ResponseEntity deleteCustomer(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
    /*void deleteCustomer(@PathVariable Long id) {
        repository.deleteById(id);
    }*/
}
