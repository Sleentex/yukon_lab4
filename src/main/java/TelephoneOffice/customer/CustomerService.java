package TelephoneOffice.customer;

import TelephoneOffice.operation.OperationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository repository;

    public CustomerDTO findById(Long id) {
        return repository.findById(id)
                .map(CustomerDTO::new)
                .orElseThrow(() -> new CustomerNotFoundException(id));
    }

    public List<CustomerDTO> findAll() {
        return repository.findAll()
                .stream()
                .map(CustomerDTO::new)
                .collect(Collectors.toList());
    }

    public CustomerDTO save(CustomerDTO customerDTO) {
        Customer customer = new Customer(customerDTO);
        return new CustomerDTO(repository.save(customer));
    }

    public CustomerDTO update(CustomerDTO customerDTO, Long id) {
        return repository.findById(id)
                .map(customer -> {
                    customer.setName(customerDTO.getName());
                    return new CustomerDTO(repository.save(customer));
                })
                .orElseGet(()->{
                    Customer customer = new Customer(customerDTO);
                    customer.setId(id);
                    return new CustomerDTO(repository.save(customer));
                });
    }

    public void deleteById(Long id) {
        if(repository.existsById(id))
            repository.deleteById(id);
        else
            throw new CustomerNotFoundException(id);
    }
}
