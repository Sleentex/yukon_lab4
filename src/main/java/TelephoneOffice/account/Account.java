package TelephoneOffice.account;

import TelephoneOffice.customer.Customer;
import TelephoneOffice.operation.Operation;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Account {
    // щоб айді могли повторюватись у різних таблицях
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //@ManyToOne(cascade = CascadeType.ALL)
    //private Customer customer;

    @ManyToMany(cascade = CascadeType.ALL)
    private Set<Operation> operations;

    private Integer price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    //public Customer getCustomer() { return customer; }

    //public void setCustomer(Customer customer) {
        //this.customer = customer;
    //}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        return id.equals(account.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
