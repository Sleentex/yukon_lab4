package TelephoneOffice.operation;

import TelephoneOffice.customer.Customer;

import javax.persistence.*;

public class OperationCreationRequest { // Для запиту на збереження

    private Long customerId;
    private Integer price;
    private String name;

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCustomerId() {
        return customerId;
    }
}