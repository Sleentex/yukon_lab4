package TelephoneOffice.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OperationController {
    //private final OperationRepository repository;
    @Autowired
    private OperationService service;

    // @Autowired
    //CustomerRepository customerRepository;

    //public OperationController(OperationRepository repository) {
     //   this.repository = repository;
    //}

    @GetMapping("/operations")
    public ResponseEntity<List<OperationDTO>> all() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findAll());
    }
    /*List<Operation> all() {
        return repository.findAll();
    }*/

    @GetMapping("/operations/{id}")
    public ResponseEntity<OperationDTO> findById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.FOUND)
                .body(service.findById(id));
    }
    /*Operation one(@PathVariable Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new OperationNotFoundException(id));
    }*/
    
    @PostMapping("/operations/save")
    public ResponseEntity<OperationDTO> newOperation(@RequestBody OperationDTO operationDTO) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(service.save(operationDTO));
    }
   /* Operation newOperation(@RequestBody OperationCreationRequest newOperation) {

        *//*Operation operationEntity = new Operation();
        operationEntity.setName(newOperation.getName());
        Customer customer = customerRepository.findById(newOperation.getCustomerId()).orElse(null);
        operationEntity.setCustomer(customer);
        return repository.save(operationEntity);*//*
    }*/

    @PutMapping("/operations/update/{id}")
    public ResponseEntity<OperationDTO> replaceOperation(@RequestBody OperationDTO operationDTO, @PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.update(operationDTO, id));
    }
   /* Operation replaceOperation(@RequestBody Operation newOperation, @PathVariable Long id) {

        return repository.findById(id)
                .map(e -> {
                    e.setName(newOperation.getName());
                    return repository.save(e);
                })
                .orElseGet(() -> {
                    newOperation.setId(id);
                    return repository.save(newOperation);
                });
    }*/

    @DeleteMapping("/operations/delete/{id}")
    public ResponseEntity deleteOperation(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
    /*void deleteOperation(@PathVariable Long id) {
        repository.deleteById(id);
    }*/
}
