package TelephoneOffice.operation;


import javax.persistence.*;
import java.util.Set;

@Entity
public class Operation {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)// щоб айді могли повторюватись у різних таблицях
    private Long id;
    private Integer price;
    private String name;

//   @OneToMany
//   private Set<Orders> orders;

    //@OneToMany(cascade = CascadeType.ALL)
    //private Set<Customer> customers;

    public Operation() {}

    public Operation(OperationDTO operationDTO) {
        if(operationDTO.getId() != null)
            this.setId(operationDTO.getId());

        //this.id = operationDTO.getId(); ??
        this.price = operationDTO.getPrice();
        this.name = operationDTO.getName();
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Operation operation = (Operation) o;

        return id.equals(operation.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Operation{" +
                "id=" + id +
                ", price=" + price +
                ", name='" + name + '\'' +
                '}';
    }
}