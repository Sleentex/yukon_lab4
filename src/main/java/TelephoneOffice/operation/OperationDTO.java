package TelephoneOffice.operation;

import TelephoneOffice.customer.Customer;
import TelephoneOffice.customer.CustomerDTO;
import org.springframework.stereotype.Component;

@Component
public class OperationDTO {
    private Long id;
    private Integer price;
    private String name;

    private CustomerDTO customer;// Що тут?

    public OperationDTO() {}

    public OperationDTO(Operation operation) {
        this.id = operation.getId();
        this.price = operation.getPrice();
        this.name = operation.getName();
    }

    public OperationDTO(String name, Integer price) {
        this.name = name;
        this.price = price;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OperationDTO that = (OperationDTO) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
