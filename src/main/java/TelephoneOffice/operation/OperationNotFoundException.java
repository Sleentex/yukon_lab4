package TelephoneOffice.operation;

public class OperationNotFoundException extends RuntimeException {
    public OperationNotFoundException(Long id) {
        super("Could not find operation " + id);
    }
}
