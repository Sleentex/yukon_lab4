package TelephoneOffice.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class OperationService {
    @Autowired
    private OperationRepository repository;

    public OperationDTO findById(Long id) {
        return repository.findById(id)
                .map(OperationDTO::new)
                .orElseThrow(()-> new OperationNotFoundException(id));
    }

    public List<OperationDTO> findAll() {
        return repository.findAll()
                .stream()
                .map(OperationDTO::new) // foreach
                .collect(Collectors.toList());
    }

    public OperationDTO save(OperationDTO operationDTO) {
        Operation operation = new Operation(operationDTO);
        return new OperationDTO(repository.save(operation));
    }

    public OperationDTO update(OperationDTO operationDTO, Long id) {
        return repository.findById(id)
                .map(operation -> {
                    operation.setName(operationDTO.getName());
                    operation.setPrice(operationDTO.getPrice());
                    return new OperationDTO(repository.save(operation));
                })
                .orElseGet(()-> {
                        Operation operation = new Operation(operationDTO);
                        operation.setId(id);
                        return new OperationDTO(repository.save(operation));
                });
    }

    public void deleteById(Long id) {
        if(repository.existsById(id))
            repository.deleteById(id);
        else
            throw new OperationNotFoundException(id);
    }

}
