package TelephoneOffice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelephoneOffice {
    public static void main(String[] args) {
        SpringApplication.run(TelephoneOffice.class, args);
    }
}
