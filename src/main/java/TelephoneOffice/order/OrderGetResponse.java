package TelephoneOffice.order;

import TelephoneOffice.customer.CustomerDTO;
import TelephoneOffice.operation.OperationDTO;

public class OrderGetResponse {
    private Long orderID;
    private OperationDTO operationDTO;
    private CustomerDTO customerDTO;

    public OrderGetResponse() {}

    public OrderGetResponse(Orders orders) {
        this.orderID = orders.getId();
        this.operationDTO = new OperationDTO(orders.getOperation());
        this.customerDTO = new CustomerDTO(orders.getCustomer());
    }

    public Long getOrderID() {
        return orderID;
    }

    public void setOrderID(Long orderID) {
        this.orderID = orderID;
    }

    public OperationDTO getOperationDTO() {
        return operationDTO;
    }

    public void setOperationDTO(OperationDTO operationDTO) {
        this.operationDTO = operationDTO;
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }
}
