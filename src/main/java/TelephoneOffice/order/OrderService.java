package TelephoneOffice.order;

import TelephoneOffice.customer.Customer;
import TelephoneOffice.customer.CustomerRepository;
import TelephoneOffice.operation.Operation;
import TelephoneOffice.operation.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {
    @Autowired
    private OrderRepository repository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private OperationRepository operationRepository;

    public OrderGetResponse findById(Long id) {
        return repository.findById(id)
                .map(OrderGetResponse::new)
                .orElseThrow(()-> new OrderNotFoundException(id));
    }

    public List<OrderGetResponse> findAll() {
        return repository.findAll()
                .stream()
                .map(OrderGetResponse::new) // foreach
                .collect(Collectors.toList());
    }

    public OrderGetResponse save(OrderCreationRequest orderCreationRequest) {
        Customer customer = customerRepository.findById(orderCreationRequest.getCustomerID())
                .orElseThrow(() -> new OrderNotFoundException(orderCreationRequest.getCustomerID()));

        Operation operation = operationRepository.findById(orderCreationRequest.getOperationID())
                .orElseThrow(() -> new OrderNotFoundException(orderCreationRequest.getOperationID()));

        Orders orders = new Orders(customer, operation);
        return new OrderGetResponse(repository.save(orders));
    }

    public OrderGetResponse update(OrderCreationRequest orderCreationRequest, Long id) {
        Customer customer = customerRepository.findById(orderCreationRequest.getCustomerID())
                .orElseThrow(() -> new OrderNotFoundException(orderCreationRequest.getCustomerID()));

        Operation operation = operationRepository.findById(orderCreationRequest.getOperationID())
                .orElseThrow(() -> new OrderNotFoundException(orderCreationRequest.getOperationID()));

        return repository.findById(id)
                .map(order -> {
                    order.setCustomer(customer);
                    order.setOperation(operation);
                    return new OrderGetResponse(repository.save(order));
                })
                .orElseGet(()-> {
                    Orders orders = new Orders(customer, operation);
                    operation.setId(id);
                    return new OrderGetResponse(repository.save(orders));
                });
    }

    public void deleteById(Long id) {
        if(repository.existsById(id))
            repository.deleteById(id);
        else
            throw new OrderNotFoundException(id);
    }
}
