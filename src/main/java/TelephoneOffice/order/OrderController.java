package TelephoneOffice.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class OrderController {
    @Autowired
    private OrderService service;

    @GetMapping("/orders")
    public ResponseEntity<List<OrderGetResponse>> all() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findAll());
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<OrderGetResponse> findById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.FOUND)
                .body(service.findById(id));
    }

    @PostMapping("/orders/save")
    public ResponseEntity<OrderGetResponse> newOrder(@RequestBody OrderCreationRequest orderCreationRequest) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(service.save(orderCreationRequest));
    }

    @PutMapping("/orders/update/{id}")
    public ResponseEntity<OrderGetResponse> replaceOrder(@RequestBody OrderCreationRequest orderCreationRequest,
                                                             @PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.update(orderCreationRequest, id));
    }

    @DeleteMapping("/orders/delete/{id}")
    public ResponseEntity deleteOrder(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }
}
