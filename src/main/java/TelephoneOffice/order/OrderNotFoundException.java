package TelephoneOffice.order;

public class OrderNotFoundException extends RuntimeException {
    public OrderNotFoundException(Long id) {
        super("Could not find operation " + id);
    }
}
