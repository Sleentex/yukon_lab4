package TelephoneOffice.order;

public class OrderCreationRequest {
    private Long customerID;
    private Long operationID;

    public OrderCreationRequest() {}

    public OrderCreationRequest(Long customerID, Long operationID) {
        this.customerID = customerID;
        this.operationID = operationID;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    public Long getOperationID() {
        return operationID;
    }

    public void setOperationID(Long operationID) {
        this.operationID = operationID;
    }
}
